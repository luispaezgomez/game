import pygame
import time
import math
# Configuración de pygame
pygame.init()
screen = pygame.display.set_mode((1280, 720))
clock = pygame.time.Clock()
font = pygame.font.Font(None, 74)
running = True
dt = 0

# Función para mostrar la pantalla de carga
def loading_screen(surface):
    surface.fill((0, 0, 0))  # Fondo negro
    text = font.render("Cargando...", True, (255, 255, 255))
    text_rect = text.get_rect(center=(surface.get_width() / 2, surface.get_height() / 2))
    surface.blit(text, text_rect)
    pygame.display.flip()
    time.sleep(2)  # Simula un tiempo de carga

# Función para dibujar un sol
def draw_sun(surface, x, y, radius):
    pygame.draw.circle(surface, (255, 223, 0), (x, y), radius)
    for i in range(12):  # Dibujar 12 rayos
        angle = math.radians(i * 30)
        start_x = x + int(radius * math.cos(angle))
        start_y = y + int(radius * math.sin(angle))
        end_x = x + int((radius + 20) * math.cos(angle))
        end_y = y + int((radius + 20) * math.sin(angle))
        pygame.draw.line(surface, (255, 223, 0), (start_x, start_y), (end_x, end_y), 2)

# Mostrar la pantalla de carga
loading_screen(screen)

# Posiciones iniciales
player_pos = pygame.Vector2(screen.get_width() / 2, screen.get_height() / 2)
enemy_pos  = pygame.Vector2(screen.get_width() / 4, screen.get_height() / 4)

while running:
    # Manejar eventos
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    # Llenar la pantalla con un color para borrar el marco anterior
    screen.fill((135, 206, 235))  # Color azul cielo

    # Dibujar el sol en el fondo
    draw_sun(screen, screen.get_width() - 100, 100, 50)

    # Dibujar el jugador como un cuadrado
    pygame.draw.rect(screen, "red", (player_pos.x - 20, player_pos.y - 20, 40, 40))

    # Manejar el movimiento del jugador
    keys = pygame.key.get_pressed()
    if keys[pygame.K_w]:
        player_pos.y -= 300 * dt
    if keys[pygame.K_s]:
        player_pos.y += 300 * dt
    if keys[pygame.K_a]:
        player_pos.x -= 300 * dt
    if keys[pygame.K_d]:
        player_pos.x += 300 * dt

    # Actualizar la pantalla
    pygame.display.flip()

    # Limitar la velocidad de fotogramas a 60 FPS
    dt = clock.tick(60) / 1000

pygame.quit()
