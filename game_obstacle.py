import pygame
import sys

# Inicializar Pygame
pygame.init()

# Configuración de la pantalla
screen_width = 800
screen_height = 600
screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption('Juego Tipo Geometry Dash')

# Colores
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)

# Propiedades del jugador
player_width = 50
player_height = 50
player_x = 100
player_y = screen_height - player_height - 100
player_vel_y = 0
jump_strength = -15
gravity = 1

# Propiedades del obstáculo
obstacle_width = 50
obstacle_height = 50
obstacle_x = screen_width
obstacle_y = screen_height - obstacle_height - 100
obstacle_speed = -5

# Reloj para controlar la tasa de fotogramas
clock = pygame.time.Clock()
fps = 60

# Bucle principal del juego
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE and player_y == screen_height - player_height - 100:
                player_vel_y = jump_strength

    # Actualizar la posición del jugador
    player_vel_y += gravity
    player_y += player_vel_y
    if player_y > screen_height - player_height - 100:
        player_y = screen_height - player_height - 100

    # Mover el obstáculo
    obstacle_x += obstacle_speed
    if obstacle_x < -obstacle_width:
        obstacle_x = screen_width

    # Comprobar colisión
    if player_x < obstacle_x + obstacle_width and player_x + player_width > obstacle_x and player_y < obstacle_y + obstacle_height and player_y + player_height > obstacle_y:
        running = False  # Fin del juego en caso de colisión

    # Dibujar todo en la pantalla
    screen.fill(WHITE)
    pygame.draw.rect(screen, BLACK, (player_x, player_y, player_width, player_height))
    pygame.draw.rect(screen, RED, (obstacle_x, obstacle_y, obstacle_width, obstacle_height))
    pygame.display.flip()

    # Controlar la tasa de fotogramas
    clock.tick(fps)

pygame.quit()
sys.exit()
  